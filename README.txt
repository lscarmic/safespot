DVA (CSE6242) Final Project - Safespot Parking Advisor
----------------------------------------------------------

DESCRIPTION 

/crime_data
Contains Jupyter notebooks that handle all data pre-processing and data analysis. Also contains .csv and .json output of the analysis divided by offense type (felony, misdemeanor, and violation). The output files are used in the final web app.

Raw data can be directly downloaded from NYC Open Data.

NYPD Complaint Data Historic: https://data.cityofnewyork.us/Public-Safety/NYPD-Complaint-Data-Historic/qgea-i56i
NYPD Complaint Data Current: https://data.cityofnewyork.us/Public-Safety/NYPD-Complaint-Data-Current-Year-To-Date-/5uac-w243
NYC Parking Lot: https://data.cityofnewyork.us/City-Government/Parking-Lot/h7zy-iq3d

Jupyter Notebooks:
- Classification.ipynb
- NYC_Crime_Data.ipynb

Some python packages are used in these notebooks. Install the package before running the jupyternotebook.
numpy, pandas, geopandas, matplotlib, descartes, shapely, sklearn, geopy
pip install any missing package, ex: "pip install numpy".

/front_end/src/app
Contains the full web app Angular framework and visualization code.

PREREQUISITES
Installation requires the latest LTS of node package manager, which can be downloaded from https://nodejs.org/

INSTALLATION
With NPM installed, open a terminal or command line to frontend directory

Run "npm install" to load the packages

EXECUTION

Run "npm start" to start the application

DEMO
https://safespot-gatech.web.app

This isn't a demo video, but a live version of the tool.
