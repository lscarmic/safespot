import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import * as L from 'leaflet';
import { GeoData } from '../services/geo-data.interface';
import { GeoDataService } from '../services/geo-data.service';
import { LocationSearchResults } from '../services/location-search.interface';
import { controlSlider } from './leaflet-slider'
import { CrimeData, CrimeFeatureData } from '../services/crime.interface';
import { CrimeDataService } from '../services/crime-data.service';
import * as d3 from 'd3';
import { PopularSearchService } from '../services/popular-search.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnDestroy, OnChanges {

  sub!: Subscription;
  errorMessage: string = '';
  error: boolean = false;
  lotData: GeoData;
  felonyData: CrimeFeatureData[] = [];
  violationData: CrimeFeatureData[] = [];
  misdemeanorData: CrimeFeatureData[] = [];
  map: any;
  overlay: any;
  overlayMaps = {
    "Destination": null,
    "Parking Lots": null,
    // "Weather": null,
    "Felony Clusters": null,
    "Misdemeanor Clusters": null,
    "Violation Clusters": null
  };
  layers = null
  lots = null
  filteredLots = null
  previousRadius = 0
  circle = null;
  slider = null;
  clusterRadius = .25
  @Input() geoLocation: LocationSearchResults;

  constructor(private geoService: GeoDataService,
    private crimeService: CrimeDataService) { }

  ngOnInit(): void {
    this.getCrimeData("felony", this.felonyData);
    this.getCrimeData("violation", this.violationData);
    this.getCrimeData("misdemeanor", this.misdemeanorData);
    this.map = L.map('mapid');

    if (this.geoLocation) {
      this.getLotData();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.geoLocation = changes['geoLocation'].currentValue
    if (this.geoLocation) {
      // this.map.removeLayer(this.lots);
      this.getLotData();
    }
  }

  getLotData(): void {
    this.sub = this.geoService.getLotData().subscribe({
      next: d => {
        this.error = false;
        this.lotData = d;
        // this.map.removeLayer(this.lots);
        this.drawMap();
      },
      error: err => {
        this.error = true;
        this.errorMessage = err;
      }
    })
  }



  getCrimeData(type: string, dataArray: CrimeFeatureData[]): void {
    this.sub = this.crimeService.getCrimeData(type).subscribe({
      next: d => {
        this.error = false;
        let csvToRowArray = d.split("\n");
        for (let index = 1; index < csvToRowArray.length - 1; index++) {
          let row = csvToRowArray[index].split(",");
          dataArray.push({
            "type": "Feature",
            "properties": { "offense": type },
            "geometry": {
              "type": "Point",
              "coordinates": [+row[4], +row[5]]
            }
          });
        }
      },
      error: err => {
        this.error = true;
        this.errorMessage = err;
      }
    })
  }


  ngOnDestroy(): void {
    this.sub.unsubscribe()
  }

  addBaseLayer() {
    L.tileLayer('https://api.mapbox.com/styles/v1/lscarmic/ckvbct6f828p314s53de7nuav/tiles/256/{z}/{x}/{y}@2x?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      // id: 'mapbox/light-v9', //https://stackoverflow.com/questions/37166172/mapbox-tiles-and-leafletjs
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoibHNjYXJtaWMiLCJhIjoiY2t2OW13Z2cyMGRlMTMxdDJ0ajRkamJnYiJ9.Sy8gK_isbVDp3N7kkvErIw'
    }).addTo(this.map);
  }

  getRadiusLots(center: any, radius: any) {

    var all_lots = this.lotData.features
    var filtered_lots = []
    var dest = L.latLng(center)
    var radius: any = 1609 * radius // miles to meters

    //for each lot in data
    loop1:
    for (const lot of all_lots) {
      var coord_set = lot.geometry.coordinates[0][0] // drill ALL the way down to the coordinates
      loop2:
      for (let coord of coord_set) {
        coord = [coord[1], coord[0]]
        if (dest.distanceTo(coord) < radius) {
          filtered_lots.push(lot)
          break loop2
        }
      };
    };
    return filtered_lots

  }

  addCircle() {
    return L.circle(this.geoLocation.geometry.location, {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.8,
      weight: 1,
      radius: 30,
      id: "locationCirle"
    }).addTo(this.map);
  }

  lot_style(feature) {
    let score = feature.properties.safety_score;
    let color = '';
    if (score < 50) { color = "#d7191c" } else
      if (score < 70) { color = "#fdae61" } else
        if (score < 90) { color = "#a6d96a" } else { color = "#1a9641" }

    return {
      color: color,
      fillColor: color,
      weight: 1,
      fillOpacity: 0.6,
      opacity: 1
    };
  }

  lot_lookup(data: any, lot_id) {
    return data.filter(d => d.properties.source_id == lot_id)
  }

  addLayers() {
    return L.control.layers(null, this.overlayMaps, {
      collapsed: false
    }).addTo(this.map);
  }

  selectLot(lot) {
    L.geoJSON(lot, {
      style: this.lot_style
    }).addTo(this.map);
  }

  getLots() {

    return L.geoJSON(this.getRadiusLots(this.geoLocation.geometry.location, this.previousRadius), { //radius_lots is a list of Feature dictionaries (one dict per lot)
      style: this.lot_style,
      className: "lots",
      onEachFeature: function (feature, layer) {
        layer.bindPopup(feature.properties.popup_html);
      },
    });
  }

  getLayers() {
    const _layers = {};
    this.layers._layers.forEach(element => {
      if (element.overlay) {
        // get name of overlay
        let layerName = element.name;
        // store whether it's present on the map or not
        return _layers[layerName] = this.layers._map.hasLayer(element.layer);
      }
    });
    return _layers
  }

  addSlider() {

    return controlSlider((value: number) => {
      this.map.removeLayer(this.lots)
      if (this.getLayers()['Parking Lots']) {
        // this.lots = L.geoJSON(this.getRadiusLots(this.geoLocation.geometry.location, this.previousRadius), { style: this.lot_style, });
        // this.map.addLayer(this.lots)
        this.lots = L.geoJSON(this.getRadiusLots(this.geoLocation.geometry.location, this.previousRadius), { //radius_lots is a list of Feature dictionaries (one dict per lot)
          style: this.lot_style,
          className: "lots",
          onEachFeature: function (feature, layer) {
            layer.bindPopup(feature.properties.popup_html);
          },
        }).addTo(this.map);
      }
      this.previousRadius = value
    }, {
      id: "slider",
      orientation: 'vertical',
      max: 6,
      min: .1,
      value: 0,
      step: 0.1,
      logo: "r",
      syncSlider: true,
      collapsed: false
    }).addTo(this.map);
  }

  // calculate lot scores and style
  // adds score and style to lot properties (Feature.properties)
  calculate_scores() {
    var crime_radius: any = 1609 * this.clusterRadius // miles to meters

    // var a_lot = this.lot_lookup(this.lotData.features, "12500000183")

    for (var lot of this.lotData.features) { //for (const lot of all_lots) {
      var lot_center = L.latLng(lot.properties.center)

      // add local felonies to lot properites
      var felony_count = 0
      for (var crime of this.felonyData) {
        if (lot_center.distanceTo(crime.geometry.coordinates) < crime_radius) {
          felony_count++
        }
      }
      lot.properties['local_felonies'] = felony_count

      // add local misdemeanors to lot properites
      var misd_count = 0
      for (var crime of this.misdemeanorData) {
        if (lot_center.distanceTo(crime.geometry.coordinates) < crime_radius) {
          misd_count++
        }
      }
      lot.properties['local_misdemeanors'] = misd_count

      // add local violoations to lot properites
      var viol_count = 0
      for (var crime of this.violationData) {
        if (lot_center.distanceTo(crime.geometry.coordinates) < crime_radius) {
          viol_count++
        }
      }

      lot.properties['local_violations'] = viol_count

      if (lot.properties.source_id == "12500000183") {
      }

      // calcluate and add lot safety score
      lot.properties['safety_score'] = (((100 - (10 * felony_count + 4 * misd_count + viol_count))))
      lot.properties['popup_html'] = "<span><b>Safety Score</b></span><br><span style='align:center; font-size:24px'><b>" + Math.trunc(lot.properties.safety_score) + "</b></span>" +
        "<hr><b>Crime Stats (Sept 2021)</b>" +
        "<br>Felonies: " + lot.properties.local_felonies +
        "<br>Midemeanors: " + lot.properties.local_misdemeanors +
        "<br>Violations: " + lot.properties.local_violations
    }
  }

  getCrimeClusters(data: any, style: any) {
    return L.geoJSON(data, { //radius_lots is a list of Feature dictionaries (one dict per lot)
      pointToLayer: function (feature, latlng) {
        // have to swap these because they come in backwards which screws everything up
        var temp = latlng['lat']
        latlng['lat'] = latlng['lng']
        latlng['lng'] = temp
        return L.circle(latlng, style).setRadius(.25 * 1609);
      }
    });
  }

  getCrimeClusterStyle(color: string, weight: number) {
    return {
      fillColor: color,
      color: color,
      weight: weight,
      opacity: 1,
      fillOpacity: weight
    }
  }

  addOverlayListeners() {
    this.map.on('overlayadd', (e) => {
      this.map.removeLayer(this.lots)
      this.lots = L.geoJSON(this.getRadiusLots(this.geoLocation.geometry.location, this.previousRadius), { //radius_lots is a list of Feature dictionaries (one dict per lot)
        style: this.lot_style,
        className: "lots",
        onEachFeature: function (feature, layer) {
          layer.bindPopup(feature.properties.popup_html);
        },
      }).addTo(this.map);
    })

    this.map.on('overlayremove', (e) => {
      if (e.name === "Parking Lots") {
        this.map.removeLayer(this.lots)
      }
    })
  }

  drawMap() {
    this.calculate_scores()
    this.addBaseLayer()
    this.map.setView(this.geoLocation.geometry.location, 15) //17
    if (this.map.hasLayer(this.circle)) {
      this.map.removeLayer(this.circle);
    }
    this.circle = this.addCircle()
    if (this.map.hasLayer(this.lots)) {
      this.map.removeLayer(this.lots);
    }
    this.lots = this.getLots();
    this.addOverlayListeners()
    this.drawOverlay();
    if (this.slider) {
      this.map.removeControl(this.slider);
    }
    this.slider = this.addSlider();

  }

  drawOverlay() {
    this.overlayMaps.Destination = this.circle
    this.overlayMaps["Parking Lots"] = this.lots
    const felonyClusterStyle = this.getCrimeClusterStyle("#2c7fb8", .1);
    const felonyClusters = this.getCrimeClusters(this.felonyData, felonyClusterStyle);
    this.overlayMaps['Felony Clusters'] = felonyClusters
    const misdemeanorClusterStyle = this.getCrimeClusterStyle("#7fcdbb", .2);
    const misdemeanorClusters = this.getCrimeClusters(this.misdemeanorData, misdemeanorClusterStyle);
    this.overlayMaps['Misdemeanor Clusters'] = misdemeanorClusters
    const violationClusterStyle = this.getCrimeClusterStyle("#edf8b1", .2);
    const violationClusters = this.getCrimeClusters(this.violationData, violationClusterStyle);
    this.overlayMaps['Violation Clusters'] = violationClusters
    if (this.layers) {
      this.map.removeControl(this.layers);
    }
    this.layers = this.addLayers();
  }
}
