import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
    '.example-spacer {flex: 1 1 auto;}',
    '.img-fluid {max-width: 4% !important}',
    '.flex {display: flex;}',
    'h1 {font-size: 1.5em; font-weight: bold;}'
  ]
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
