export interface CrimeData {
    "Date": string;
    "Time": string;
    "Level_of_offense": string;
    "Latitude": number;
    "Longitude": number
}

export interface CrimeFeatureData {
    "type": string;
    "properties": FeatureProperty;
    "geometry": GeometryFeatureData;
}

export interface FeatureProperty {
    "offense": string;
}

export interface GeometryFeatureData {
    "type": string;
    "coordinates": number[];
}