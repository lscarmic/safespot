import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CrimeDataService {

  private misdemeanorPath = "./assets/df_misdemeanor.csv";
  private violationPath = "./assets/df_violation.csv";
  private felonyPath = "./assets/df_felony.csv";
  private paths = {
    "felony": this.felonyPath,
    "misdemeanor": this.misdemeanorPath,
    "violation": this.violationPath
  }
  errorMessage = '';

  constructor(private http: HttpClient) { }

  getCrimeData(type: string): Observable<any> {
    let path = this.paths[type]
    return this.http.get(path, { responseType: 'text' })
      .pipe(data => data,
        catchError(this.handleError)
      );
  }

  handleError(err: HttpErrorResponse) {
    this.errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      this.errorMessage = `An Error occurred: ${err.error.message}`
    } else {
      this.errorMessage = `Server returned code: ${err.status}, error message is: ${err.error.message}`
    }
    console.error(this.errorMessage);
    return throwError(this.errorMessage);
  }
}
