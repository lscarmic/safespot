export interface LocationSearchResponse {
    results: Array<LocationSearchResults>;
    status: string;
}

export interface LocationSearchResults {
    address_components: Array<AddressComponent>;
    formatted_address: string;
    geometry: Geometry;
    place_id: string;
    types: Array<string>;
}

export interface AddressComponent {
    long_name: string;
    short_name: string;
    types: Array<string>
}

export interface Geometry {
    bounds: NESW;
    location: LatLng;
    loction_type: string;
    viewport: NESW;
}

export interface NESW {
    northeast: LatLng;
    southwest: LatLng;
}

export interface LatLng {
    lat: number;
    lng: number;
}
