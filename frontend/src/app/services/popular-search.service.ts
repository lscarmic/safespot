import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopularSearchService {

  constructor() { }

  sendLocation: BehaviorSubject<string> = new BehaviorSubject('665 W 34th St, New York, NY 10001, USA');
  sendLocation$ = this.sendLocation.asObservable();
}
