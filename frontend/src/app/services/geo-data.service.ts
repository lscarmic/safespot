import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, catchError, throwError, tap } from 'rxjs';
import { GeoData } from './geo-data.interface';

@Injectable({
  providedIn: 'root'
})
export class GeoDataService {

  private path = "./assets/lots_centers.geojson";
  errorMessage = '';

  constructor(private http: HttpClient) { }

  getLotData(): Observable<any> {
    return this.http.get<GeoData>(this.path)
      .pipe(data => data,
        catchError(this.handleError)
      );
  }

  handleError(err: HttpErrorResponse) {
    this.errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      this.errorMessage = `An Error occurred: ${err.error.message}`
    } else {
      this.errorMessage = `Server returned code: ${err.status}, error message is: ${err.error.message}`
    }
    console.error(this.errorMessage);
    return throwError(this.errorMessage);
  }
}
