import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError, tap } from 'rxjs';
import { LocationSearchResponse } from './location-search.interface';

@Injectable({
  providedIn: 'root'
})
export class LocationSearchService {
  private url = 'https://maps.googleapis.com/maps/api/geocode/json';
  errorMessage = '';
  apiKey = 'AIzaSyAF4Rio3XCO_a3fiB1H2gBf4V8rHjqhDTo'

  constructor(private http: HttpClient) { }

  getGeoLocation(location: string): Observable<LocationSearchResponse> {
    const params = new HttpParams()
      .set('address', location)
      .set('key', this.apiKey)
    return this.http.get<LocationSearchResponse>(this.url, { params: params })
      .pipe(data => data,
        catchError(this.handleError)
      );
  };

  handleError(err: HttpErrorResponse) {
    this.errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      this.errorMessage = `An Error occurred: ${err.error.message}`;
    } else {
      this.errorMessage = `Server returned code: ${err.status}, error message is: ${err.error.message}`;
    }
    console.error(this.errorMessage);
    return throwError(this.errorMessage);
  };
}
