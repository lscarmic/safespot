
import { Component, OnInit, ViewChild } from '@angular/core';
import { PopularSearchService } from '../services/popular-search.service';

@Component({
  selector: 'app-popular-search',
  templateUrl: './popular-search.component.html',
  styleUrls: ['./popular-search.component.css']
})
export class PopularSearchComponent implements OnInit {
  public isScreenSmall: Boolean;
  constructor(private popularSearchService: PopularSearchService) { }

  ngOnInit(): void {
  }

  sendLocation(location: string) {
    console.log("Popular Search Component: " + location);
    this.popularSearchService.sendLocation.next(location);

  }
}
