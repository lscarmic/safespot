import { Component } from '@angular/core';
import { LocationSearchResults } from './services/location-search.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  geoLocation: LocationSearchResults;


  getLocation(geoLocation: LocationSearchResults) {
    this.geoLocation = geoLocation;
  }
}
