import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LocationSearchResults } from '../services/location-search.interface';
import { LocationSearchService } from '../services/location-search.service';
import { PopularSearchService } from '../services/popular-search.service';

@Component({
  selector: 'app-location-search',
  templateUrl: './location-search.component.html',
  styleUrls: ['./location-search.component.css'],
})
export class LocationSearchComponent implements OnInit, OnDestroy {
  @Output() locationEvent = new EventEmitter<LocationSearchResults>();

  geoLocation: LocationSearchResults;

  location: string = '';
  sub!: Subscription

  constructor(private locationService: LocationSearchService,
    private popularLocationService: PopularSearchService) { }

  ngOnInit(): void {
    this.popularLocationService.sendLocation$.subscribe(
      location => {
        this.location = location
        this.getLocation()
      }
    )

  }

  onSubmit(form: NgForm) {
    this.getLocation()
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  };

  getLocation() {
    this.sub = this.locationService.getGeoLocation(this.location).subscribe({
      next: data => {
        if (data.status == 'OK') {
          this.geoLocation = data.results[0]
          this.locationEvent.emit(this.geoLocation);
        }
      }
    })
  };

}
