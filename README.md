# DVA (CSE6242) Final Project - Safespot Parking Advisor

## DESCRIPTION - Describe the package in a few paragraphs

### /crime_data
Contains Jupyter notebooks that handle all data pre-processing and data analysis. Also contains .csv and .json output of the analysis divided by offense type (felony, misdemeanor, and violation). The output files are used in the final web app.

Jupyter Notebooks:
- Classification.ipynb - 
- NYC_Crime_Data.ipynb - 


### /front_end/src/app
Contains the full web app Angular framework and visualization code.

### /back_end

## INSTALLATION - How to install and setup your code

## EXECUTION - How to run a demo on your code

## DEMO

link

[Optional, but recommended] DEMO VIDEO - Include the URL of a 1-minute *unlisted* YouTube video in this txt file. The video would show how to install and execute your system/tool/approach (e.g, from typing the first command to compile, to system launching, and running some examples). Feel free to speed up the video if needed (e.g., remove less relevant video segments). This video is optional (i.e., submitting a video does not increase scores; not submitting one does not decrease scores). However, we recommend teams to try and create such a video, because making the video helps teams better think through what they may want to write in the README.txt, and generally how they want to "sell" their work.
